# 笔记-Android开发环境
 - [Android 查看项目依赖树的四种方式](https://juejin.cn/post/6990156937165799454)  
 - [将 build 配置从 Groovy 迁移到 KTS](https://developer.android.google.cn/studio/build/migrate-to-kts?hl=zh-cn)
 - [Gradle - Java - Kotlin版本对应关系](https://docs.gradle.org/current/userguide/compatibility.html)  
 - [JDK下载](https://jdk.java.net/21/)
 - [Android 动态权限申请框架easypermissions使用详解](https://blog.csdn.net/mqdxiaoxiao/article/details/103842868)

## 在settings.gradle添加aliyun仓库
  - Kotlin DSL  
    ```Kotlin
    pluginManagement {
        repositories {
            maven {
                isAllowInsecureProtocol = true
                setUrl("https://maven.aliyun.com/repository/public/")
            }
            maven {
                isAllowInsecureProtocol = true
                setUrl("https://maven.aliyun.com/repository/google")
            }
            maven {
                isAllowInsecureProtocol = true
                setUrl("https://maven.aliyun.com/repository/central")
            }
            maven {
                isAllowInsecureProtocol = true
                setUrl("https://maven.aliyun.com/repository/gradle-plugin")
            }
            google()
            mavenCentral()
            gradlePluginPortal()
        }
    }
    dependencyResolutionManagement {
        repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
        repositories {
            maven {
                isAllowInsecureProtocol = true
                setUrl("https://maven.aliyun.com/repository/public/")
            }
            maven {
                isAllowInsecureProtocol = true
                setUrl("https://maven.aliyun.com/repository/google")
            }
            maven {
                isAllowInsecureProtocol = true
                setUrl("https://maven.aliyun.com/repository/central")
            }
            google()
            mavenCentral()
        }
    }
    ```
  - Groovy DSL
    ```groovy
    pluginManagement {
        repositories {
            maven{ url 'https://maven.aliyun.com/repository/public'}
            maven{ url 'https://maven.aliyun.com/repository/google'}
            maven{ url 'https://maven.aliyun.com/repository/central'}
            maven{ url 'https://maven.aliyun.com/repository/gradle-plugin'}
            google()
            mavenCentral()
            gradlePluginPortal()
        }
    }
    dependencyResolutionManagement {
        repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
        repositories {
            maven{ url 'https://maven.aliyun.com/repository/public'}
            maven{ url 'https://maven.aliyun.com/repository/google'}
            maven{ url 'https://maven.aliyun.com/repository/central'}
            google()
            mavenCentral()
        }
    }    
    ```

## Gradle Plugin 与 Gradle 版本、JDK 版本对应关系
|    Gradle Plugin版本    |     Gradle版本   | SDK Build Tools | JDK版本 |
|-------------------------|-----------------|-----------------|-------|
| 8.2                     | 8.1             |                 | 17    |
| 8.1                     | 8               |                 | 17    |
| 8                       | 8               | 30.0.3          | 17    |
| 7.4                     | 7.5             | 30.0.3          | 11    |
| 7.3                     | 7.4             | 30.0.3          | 11    |
| 7.2                     | 7.3.3           | 30.0.3          | 11    |
| 7.1                     | 7.2             | 30.0.3          | 11    |
| 7                       | 7               | 30.0.2          | 11    |
|4.2.0+	                  | 6.7.1	    | 30.0.2	      | 8+    |
|4.1.0+	                  | 6.5+	    | 29.0.2	      | 8+    |
|4.0.0+	                  | 6.1.1+	    | 29.0.2	      | 8+    |
|3.6.0 - 3.6.4	          | 5.6.4+	    | 28.0.3	      | 8+    |
|3.5.0 - 3.5.4	          | 5.4.1+	    | 28.0.3	      | 8+    |
|3.4.0 - 3.4.3	          | 5.1.1+	    | 28.0.3	      | 8+    |
|3.3.0 - 3.3.3	          | 4.10.1+	    | 28.0.3	      | 8+    |
|3.2.0 - 3.2.1	          | 4.6+	    | 28.0.3	      | 8+    |
|3.1.0+	                  | 4.4+	    | 27.0.3	      | 8+    |
|3.0.0+	                  | 4.1+	    | 26.0.2	      | 8+    |
|2.3.0+	                  | 3.3+	    | 25.0.0	      | 8+    |
|2.1.3 - 2.2.3	          | 2.14.1 - 3.5    | 23.0.2	      | 7+    |
|2.0.0 - 2.1.2	          | 2.10 - 2.13	    | 21.1.1	      | 7+    |
|1.5.0	                  | 2.2.1 - 2.13    | 21.1.1	      | 7+    |
|1.2.0 - 1.3.1	          | 2.2.1 - 2.9	    | 21.1.1	      | 7+    |
|1.0.0 - 1.1.3	          | 2.2.1 - 2.3	    | 21.1.1	      | 7+    |
