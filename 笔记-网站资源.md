# 笔记-网站资源
## Flutter
- [Flutter 官网](https://flutter.dev)    [(Flutter 官网-中文)](https://flutter.cn)  
- [pub.dev 官方组件库](https://pub.dev)    [(pub.dev 官方组件库-国内镜像)](https://pub-web.flutter-io.cn)  
- [Dart 官网](https://dart.dev)    [(Dart 官网-中文)](https://dart.cn)  
- [Dart 在线编辑器](https://dartpad.dev)    [(Dart 在线编辑器-中文)](https://dartpad.cn)  
- [Flutter实战 第二版](https://github.com/CarGuo/gsy_flutter_demo)  
- [fluttercandies社区](https://fluttercandies.com/)
- [fluttergems插件分类](https://fluttergems.dev/)
- [Flutter插件分类](https://flutter.ducafecat.com/)

## Android&Kotlin
- [Aandroid 开发者官网-中文](https://developer.android.google.cn/?hl=zh-cn)  
- [Aandroid 示例](https://github.com/android/nowinandroid)  
- [Kotlin 官方文档-中文](https://book.kotlincn.net)  
- [Kotlin 在线编辑器](https://play.kotlinlang.org/)  
- [Android Kotlin 官方-中文](https://developer.android.google.cn/kotlin?hl=zh-cn)  
- [Android 之 Compose 开发基础](https://developer.android.google.cn/kotlin/androidbasics?hl=zh-cn)  
- [将 Kotlin 添加到现有Android应用](https://developer.android.google.cn/kotlin/add-kotlin?hl=zh-cn)  
- [Android KTX](https://developer.android.google.cn/kotlin/ktx?hl=zh-cn)  
- [Android 上的 Kotlin 协程](https://developer.android.google.cn/kotlin/coroutines?hl=zh-cn)  

## Others
- [Gradle下载](https://services.gradle.org/distributions/)
- [JavaSE / JDK下载](https://jdk.java.net/java-se-ri/18)
- [GitHub 全球最大开源网站](https://github.com)  
- [Gitee 国内开源网站](https://gitee.com)  
- [GitHub 中文排行榜](https://github.com/GrowingGit/GitHub-Chinese-Top-Charts)  
- [Json模型转换](https://app.quicktype.io/)
- [移动端(Android、Flutter)面试题](https://gitee.com/conghaonet/interview-questions)  
- [HarmonyOS第一课|视频教程学习|HarmonyOS应用开发官网](https://developer.harmonyos.com/cn/documentation/teaching-video)  

## 仓库
- [阿里云仓库](https://maven.aliyun.com)  
- [Maven Central](https://central.sonatype.com)  
- [组件库查询](https://mvnrepository.com)  

## AI（GPT）
- [百度-文心一言](https://yiyan.baidu.com)  
- [字节-豆包](https://doubao.com)  
- [阿里-通义千问](https://tongyi.aliyun.com/)  
- [Baidu Comate - AndroidStudio的AI插件](https://comate.baidu.com)  