# 如何在Android的RecyclerView中实现点击事件，并在Activity中实现回调 - JAVA代码示例

在Android中，为RecyclerView实现item点击事件并回调到Activity中，通常可以创建一个接口并在Adapter中定义它，然后在Activity中实现这个接口。以下是一个简单的Java代码示例：

首先，在Adapter中创建一个接口：

```java
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    
    // 定义点击事件接口
    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    private OnItemClickListener listener;

    // 构造函数中接收Activity或Fragment传递过来的监听器实例
    public MyAdapter(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // 设置点击事件
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 当item被点击时，通过接口回调给Activity
                if (listener != null) {
                    listener.onItemClick(position);
                }
            }
        });

        // ... 其他设置数据的方法
    }

    // ViewHolder类
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // ... ViewHolder中的View成员变量和构造函数
    }
}
```

然后，在你的Activity中实现这个接口，并将实现类的对象传入Adapter：

```java
public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener {

    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new MyAdapter(this); // 将Activity作为监听器实例传入Adapter
        mRecyclerView.setAdapter(mAdapter);

        // 实现点击事件回调方法
        @Override
        public void onItemClick(int position) {
            Toast.makeText(MainActivity.this, "Clicked on item at position: " + position, Toast.LENGTH_SHORT).show();
            // 在这里根据position处理点击事件
        }
    }
}
```

这样，当RecyclerView中的任意一个item被点击时，就会触发`onItemClick`回调方法，并在Activity中执行相应的逻辑。